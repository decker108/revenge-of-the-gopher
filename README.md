# Revenge of the Gopher

A side scrolling arcade shoot-em-up written in Go. The original game was written during the 2015 Gopher Gala by Marcus Olsson. Ola Rende and Ivar Gaitan.

![Image of Game](http://miff.hacked.jp/jamwiki-1.3/uploads/en/2015/2/revenge-of-the-gopher-screenshot-23202559.png)

Pull requests welcome!

TODO:
- More enemy types
  - Hunter killers
  - Sprayers
  - 'Sploders
- Boss fights
- Second level
- Use a markup language to create waves
- Better graphics (gimme a hand?)
- Support for using Android phones as controllers (future)
- Button to recheck for players

Known issues:
- Hit detection for enemies sometimes fails for a while

Dependencies:
- SDL 2.0.0
- SDL Mixer 2.0.0
- SDL TTF 2.0.0
- SDL Image 2.0.0
