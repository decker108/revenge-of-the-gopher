package main

import "github.com/veandco/go-sdl2/sdl"

type animState int

const (
	moveLeft  animState = 0
	moveRight animState = 1
	dying     animState = 2
	dead      animState = 3
)

type animation struct {
	frameIndex    int32
	maxFrameIndex int32
	lastFrameTick uint32
	frameInterval uint32
	startTime     uint32
	duration      uint32
	spriteSheet   *spriteSheet
}

func (e *animation) getLoopingAnimFrameOffset() int32 {
	if sdl.GetTicks()-e.lastFrameTick > e.frameInterval {
		e.frameIndex = e.frameIndex + 1
		if e.frameIndex >= e.maxFrameIndex {
			e.frameIndex = 0
		}
		e.lastFrameTick = sdl.GetTicks()
	}
	return e.frameIndex
}

func (e *animation) getLinearAnimFrameOffset() int32 {
	if sdl.GetTicks()-e.lastFrameTick > e.frameInterval {
		if e.frameIndex < e.maxFrameIndex {
			e.frameIndex = e.frameIndex + 1
			e.lastFrameTick = sdl.GetTicks()
		}
	}
	return e.frameIndex
}

type spriteSheet struct {
	texture         *sdl.Texture
	clipSize        int32
	horizontalClips int
	verticalClips   int
}

func newSpriteSheet(texture *sdl.Texture, clipSize int32, hClips, vClips int) *spriteSheet {
	return &spriteSheet{
		texture:         texture,
		clipSize:        clipSize,
		horizontalClips: hClips,
		verticalClips:   vClips,
	}
}
