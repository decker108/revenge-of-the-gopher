package main

import (
	"container/list"
	"math"
	"math/rand"
	"time"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/sdl_ttf"
)

type theGame struct {
	Wnd         *sdl.Window
	S           *scene
	R           *sdl.Renderer
	background  background
	A           map[string]*sdl.Texture
	sprites     map[string]*spriteSheet
	W           *wave
	curState    gameState
	defaultFont *ttf.Font
	players     []*entity
}

type gameState int

const (
	prestart gameState = 0
	running  gameState = 1
	gameover gameState = 2
	restart  gameState = 3
	quit     gameState = 4
)

type movementPattern int

const (
	playerControl movementPattern = -1
	straightDown  movementPattern = 0
	sine          movementPattern = 1
	saw           movementPattern = 2
	leftEdgeDown  movementPattern = 3
	rightEdgeDown movementPattern = 4
	center        movementPattern = 5
	leftEdgeStay  movementPattern = 6
	rightEdgeStay movementPattern = 7
)

type entity struct {
	pos      vector
	startX   float64
	phase    float64 //phase used for sinus wave movement
	width    int32
	height   int32
	velocity float64
	life     int32
	dead     bool
	texture  *sdl.Texture
	score    int
	hostile  bool

	lastProjectileTick uint32
	rateOfFire         uint32
	projDamage         int32
	projColor          sdl.Color
	projVelocity       float64

	movtPattern      movementPattern
	hasActivePowerup bool
	onHit            func(e *entity, damage int32)
	onFire           func(e *entity, v vector)
	onDrawGFX        func() //called before drawing entity texture

	curAnimState animState
	moveAnim     animation
	deathAnim    animation

	deathSfx      func()
	hitCountdown  uint32
	powerUpBehave powerUpBehavior
}

func newPlayer() *entity {
	return newPlayerAtPos(43, 700)
}

func newPlayerAtPos(x, y float64) *entity {
	deathAnimation := animation{ //TODO move to animation.go when sprite sheet format is done
		frameIndex:    0,
		maxFrameIndex: 2,
		lastFrameTick: 0,
		frameInterval: 200,
		duration:      2000,
		startTime:     0,
		spriteSheet:   game.sprites[exploFramesImgPath],
	}
	return &entity{
		pos:          vector{x, y},
		width:        43,
		height:       67,
		velocity:     10,
		dead:         false,
		curAnimState: moveRight,
		rateOfFire:   150,
		projDamage:   35,
		projColor:    sdl.Color{R: 0, G: 255, B: 255, A: 255},
		projVelocity: 1.2,
		deathAnim:    deathAnimation,
		onHit: func(e *entity, damage int32) {
			e.killEntity()
		},
		onFire: func(e *entity, v vector) {
			now := sdl.GetTicks()
			if (now - e.lastProjectileTick) > e.rateOfFire {
				game.S.addProjectile(e, v)
				gameAudio.playLaser()
				e.lastProjectileTick = now
			}
		},
		deathSfx: func() {
			gameAudio.playPlayerDeath()
		},
		movtPattern: playerControl,
		onDrawGFX: func() {
			return
		},
	}
}

func newEnemy() *entity {
	deathAnimation := animation{ //TODO move to animation.go when sprite sheet format is done
		frameIndex:    0,
		maxFrameIndex: 2,
		lastFrameTick: 0,
		frameInterval: 200,
		duration:      1000,
		startTime:     0,
		spriteSheet:   game.sprites[exploFramesImgPath],
	}
	moveAnimation := animation{
		frameIndex:    0,
		maxFrameIndex: 3,
		lastFrameTick: 0,
		frameInterval: 150,
		spriteSheet:   game.sprites[enemy1FramesImgPath],
	}
	return &entity{
		pos:          vector{0, 0},
		width:        121,
		height:       100,
		velocity:     5,
		life:         100,
		dead:         false,
		hostile:      true,
		texture:      game.sprites[enemy1FramesImgPath].texture,
		score:        500,
		rateOfFire:   150,
		projColor:    sdl.Color{R: 0, G: 255, B: 0, A: 255},
		projVelocity: 5.0,
		curAnimState: moveRight,
		moveAnim:     moveAnimation,
		deathAnim:    deathAnimation,
		onHit: func(enemy *entity, damage int32) {
			enemy.life = enemy.life - damage
			enemy.hitCountdown = sdl.GetTicks()
			gameAudio.playEnemyHit()

			if enemy.life <= 0 {
				enemy.killEntity()
				if game.S.isNextPowerUpDue() {
					game.S.addPowerUp(enemy.pos.x, enemy.pos.y)
				}
			}
		},
		onFire: func(e *entity, v vector) {
			now := sdl.GetTicks()
			if (now - e.lastProjectileTick) > e.rateOfFire {
				game.S.addProjectile(e, v)
				e.lastProjectileTick = now
			}
		},
		deathSfx: func() {
			gameAudio.playEnemyDeath()
		},
		movtPattern: straightDown,
	}
}

func (e *entity) moveLeft() {
	e.curAnimState = moveLeft
	e.pos.x = e.pos.x - e.velocity
}

func (e *entity) moveRight() {
	e.curAnimState = moveRight
	e.pos.x = e.pos.x + e.velocity
}

func (e *entity) moveUp() {
	e.pos.y = e.pos.y - e.velocity
}

func (e *entity) moveDown() { //TODO replace with closures
	switch e.movtPattern {
	case playerControl:
		e.pos.y = e.pos.y + e.velocity
	case straightDown:
		e.pos.y = e.pos.y + e.velocity
	case sine:
		e.pos.y = e.pos.y + e.velocity

		t := float64(time.Now().UnixNano()) / 1000000000.0
		e.pos.x = e.startX + 100*math.Sin(math.Pi*t+e.phase)
	default:
		e.pos.y = e.pos.y + e.velocity
	}
}

func (e *entity) move(left, right, up, down bool) {
	if e.dead {
		return
	}

	if e.pos.y-e.velocity < -10 {
		up = false
	}
	if int32(e.pos.y+e.velocity) > windowHeight-e.height {
		down = false
	}
	if e.pos.x-e.velocity < 0 {
		left = false
	}
	if int32(e.pos.x+e.velocity) > windowWidth-40 {
		right = false
	}

	dx, dy := 0.0, 0.0
	v_xy := float64(e.velocity)
	if (left || right) && (up || down) {
		if right {
			e.curAnimState = moveRight
			dx = v_xy / math.Sqrt(2)
		}
		if left {
			e.curAnimState = moveLeft
			dx = dx + (1-v_xy)/math.Sqrt(2)
		}
		if up {
			dy = dy + (1-v_xy)/math.Sqrt(2)
		}
		if down {
			dy = v_xy / math.Sqrt(2)
		}
	} else {
		if right {
			e.curAnimState = moveRight
			dx = v_xy
		}
		if left {
			e.curAnimState = moveLeft
			dx = dx + (1 - v_xy)
		}
		if up {
			dy = dy + (1 - v_xy)
		}
		if down {
			dy = v_xy
		}
	}

	e.pos.x += dx
	e.pos.y += dy
}

func (e *entity) fireProjectileInDir(v vector) {
	if !e.dead {
		e.onFire(e, v)
	}
}

func (e *entity) fireProjectile() {
	if !e.dead {
		e.onFire(e, vector{0, -10})
	}
}

func (e *entity) boundingRect() *sdl.Rect {
	return &sdl.Rect{int32(e.pos.x), int32(e.pos.y), e.width, e.height}
}

func (e *entity) center() vector {
	b := e.boundingRect()
	return vector{float64(b.X + b.W/2), float64(b.Y + b.H/2)}
}

func (e *entity) isBelowWindow() bool {
	return int32(e.pos.y) > windowHeight
}

func (e *entity) killEntity() {
	e.dead = true
	if e.hostile {
		game.S.hiscore = game.S.hiscore + e.score
		e.movtPattern = straightDown
	}
	e.deathSfx()
	e.curAnimState = dying
	e.deathAnim.startTime = sdl.GetTicks()
}

func (e *entity) findClosestPlayer(players []*entity) *entity {
	var d float64 = 1000.0
	var closest *entity
	for _, p := range players {
		if !p.dead && p.pos.y > e.pos.y {
			l := diff(p.pos, e.pos).length()
			if l < d {
				d = l
				closest = p
			}
		}
	}

	return closest
}

type projectile struct {
	pos          vector
	damage       int32
	velocity     float64
	width        int32
	height       int32
	direction    vector
	origin       *entity
	playerOrigin bool
	color        *sdl.Color
	move         func(p *projectile)
	timeToLive   uint32
}

func newProjectile(d vector, g *entity) *projectile {
	vel := 1.2
	if g.hostile {
		vel = 1.5
	}
	p := projectile{
		pos:          g.center(),
		damage:       g.projDamage,
		velocity:     vel,
		width:        10,
		height:       10,
		direction:    d,
		origin:       g,
		playerOrigin: !g.hostile,
		timeToLive:   0,
		color:        &g.projColor,
		move: func(p *projectile) {
			pos := vector{float64(p.pos.x), float64(p.pos.y)}
			res := add(pos, vector{p.direction.x, p.direction.y})
			p.pos.x = res.x
			p.pos.y = res.y
		},
	}
	p.direction = scale(vector{p.direction.x, p.direction.y}, p.velocity)
	return &p
}

func (p *projectile) boundingRect() *sdl.Rect {
	return &sdl.Rect{int32(p.pos.x), int32(p.pos.y), p.width, p.height}
}

func (p *projectile) isInsideWindow() bool {
	return p.pos.y > 0 && p.pos.y < float64(windowHeight) &&
		p.pos.x > 0 && p.pos.x < float64(windowWidth)
}

func (p *projectile) tick() {
	p.move(p)
}

type background struct {
	offsetY int32
	width   int32
	height  int32
	texture *sdl.Texture
}

func (b *background) scroll() {
	b.offsetY = b.offsetY - 2

	_, _, _, h, _ := b.texture.Query()
	if b.offsetY < -int32(h) {
		b.offsetY = 0
	}
}

type scene struct {
	projectiles      *list.List
	enemies          []*entity
	powerups         []*entity
	lastPowerUpTime  uint32
	hiscore          int
	newHiscore       bool
	gameStateTextTex *sdl.Texture
}

func newScene() *scene {
	rand.Seed(time.Now().UnixNano())
	return &scene{
		projectiles:     list.New(),
		enemies:         make([]*entity, 0),
		powerups:        make([]*entity, 1),
		lastPowerUpTime: sdl.GetTicks(),
		hiscore:         0,
	}
}

func (s *scene) addProjectile(e *entity, d vector) {
	s.projectiles.PushBack(newProjectile(d, e))
}
