package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/veandco/go-sdl2/sdl_mixer"
)

var gameAudio audio

type audio struct {
	noSound                                   bool
	explode1, laser1, playerDeath             *mix.Chunk
	enemyHit, powerUp1Active, powerUp1Spawned *mix.Chunk
	intro                                     *mix.Music
	bgmusicArr                                []*mix.Music
}

func initAudio(noSound bool) {
	mix.OpenAudio(22050, mix.DEFAULT_FORMAT, 2, 4096)
	mix.AllocateChannels(8)

	gameAudio = audio{
		noSound: noSound,

		explode1:        loadSfxFile("assets/SFX_Explosion_01.ogg"),
		laser1:          loadSfxFile("assets/Laser1.ogg"),
		playerDeath:     loadSfxFile("assets/player_death.ogg"),
		enemyHit:        loadSfxFile("assets/enemy_hit.ogg"),
		powerUp1Active:  loadSfxFile("assets/powerup1_active.ogg"),
		powerUp1Spawned: loadSfxFile("assets/powerup1_spawned.ogg"),

		intro: loadMusicIfFound("assets/music/intro1.ogg"),

		bgmusicArr: make([]*mix.Music, 0),
	}

	loadAllBGM("assets/music/")

	gameAudio.enemyHit.Volume(100)
	gameAudio.laser1.Volume(20)
	gameAudio.playerDeath.Volume(75)
}

func loadSfxFile(filepath string) *mix.Chunk {
	sfx, err := mix.LoadWAV(filepath)
	if err != nil {
		goFatal(err)
	}
	return sfx
}

func loadAllBGM(musicFolder string) {
	files, _ := ioutil.ReadDir(musicFolder)
	for _, f := range files {
		if strings.LastIndex(f.Name(), "intro") > -1 {
			continue
		}
		musicTrack := loadMusicIfFound(fmt.Sprintf("%s%s", musicFolder, f.Name()))
		gameAudio.bgmusicArr = append(gameAudio.bgmusicArr[:], musicTrack)
	}
}

func logFailures(result bool) {
	if result == false {
		pc, file, line, _ := runtime.Caller(1)
		fname := filepath.Base(file)
		funcName := runtime.FuncForPC(pc).Name()
		log.Printf("Audio play failure at %s:%d:%s, result: %t\n", fname, line, funcName, result)
	}
}

func loadMusicIfFound(filepath string) *mix.Music {
	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		log.Println("No such file or directory:", filepath)
		return nil
	}
	music, err := mix.LoadMUS(filepath)
	if err != nil {
		goFatal(err)
	}
	return music
}

func playOnChannel(a, b int, chunk *mix.Chunk) {
	if !gameAudio.noSound {
		chunk.PlayChannel(a, b)
	}
}

func (gameAudio *audio) playEnemyDeath() {
	playOnChannel(1, 0, gameAudio.explode1)
}

func (gameAudio *audio) playPowerUpActive() {
	playOnChannel(2, 0, gameAudio.powerUp1Active)
}

func (gameAudio *audio) playPowerUpSpawned() {
	playOnChannel(2, 0, gameAudio.powerUp1Spawned)
}

func (gameAudio *audio) playEnemyHit() {
	playOnChannel(3, 0, gameAudio.enemyHit)
}

func (gameAudio *audio) playLaser() {
	playOnChannel(0, 0, gameAudio.laser1)
}

func (gameAudio *audio) playPlayerDeath() {
	playOnChannel(1, 0, gameAudio.playerDeath)
}

func (gameAudio *audio) loopIntroMusic() {
	if gameAudio.intro != nil && !gameAudio.noSound {
		gameAudio.intro.Play(-1)
	}
}

func (gameAudio *audio) loopBGM(music *mix.Music, numLoops int) {
	if music != nil {
		music.Play(numLoops)
	}
}

func (gameAudio *audio) loopBackgroundMusic() {
	if !gameAudio.noSound {
		idx := rand.Intn(len(gameAudio.bgmusicArr))
		gameAudio.bgmusicArr[idx].Play(-1)
	}
}

func playChunk(c *mix.Chunk) {
	c.PlayChannel(1, 0)
}

func loopChunk(c *mix.Chunk) {
	c.PlayChannel(1, -1)
}

func fadeOutAll() {
	mix.FadeOutChannel(-1, 500)
}
