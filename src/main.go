package main

import (
	"errors"
	"log"
	"os"
	"path/filepath"
	"runtime"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/sdl_ttf"
)

var (
	windowTitle                     = "Revenge of the Gopher"
	windowWidth, windowHeight int32 = 1024, 768
	game                      *theGame
)

func main() {
	noSound, windowedMode, keyboardPlayers := false, false, 1
	if len(os.Args) > 1 {
		for _, arg := range os.Args[1:] {
			if arg == "-nosound" {
				noSound = true
				log.Println("Sound deactivated")
			} else if arg == "-window" || arg == "-windowed" {
				windowedMode = true
				log.Println("Windowed mode")
			} else if arg == "-nokeyboard" || arg == "-nokbplayers" {
				keyboardPlayers = 0
				log.Println("No keyboard player")
			} else if arg == "-twokbplayers" {
				keyboardPlayers = 2 //TODO implement support for multiple keyb players
				log.Println("Two keyboard players")
			}
		}
	}

	err := sdl.Init(sdl.INIT_JOYSTICK | sdl.INIT_AUDIO | sdl.INIT_GAMECONTROLLER)
	if err != nil {
		goFatal(err)
	}
	runtime.LockOSThread()

	if ttf.Init() != 0 {
		goFatal(errors.New("Error initializing TTF"))
	}

	//Initialization for reusable, stateless assets
	initAudio(noSound)
	window, renderer := initVideo(windowedMode)
	assets := setupAssets(renderer, windowedMode)
	loadHiscores()

	firstRound := true
	for true {
		mainLoop(window, renderer, assets, firstRound, keyboardPlayers)
		if diffHiscores() {
			writeHiscores()
		}
		if game.curState == restart {
			firstRound = false
		} else {
			break
		}
	}

	log.Println("Quitting...")
}

func initVideo(windowedMode bool) (*sdl.Window, *sdl.Renderer) {
	var flags uint32 = sdl.WINDOW_SHOWN
	if !windowedMode {
		flags = flags | sdl.WINDOW_FULLSCREEN_DESKTOP
	}
	window, err := sdl.CreateWindow(windowTitle, sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED, int(windowWidth), int(windowHeight), flags)
	if err != nil {
		goFatal(err)
	}

	windowW, windowH := window.GetSize()
	windowWidth, windowHeight = int32(windowW), int32(windowH)
	log.Println("w:", windowW, " h:", windowH)

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		goFatal(err)
	}

	sdl.ShowCursor(0)

	return window, renderer
}

func mainLoop(window *sdl.Window, renderer *sdl.Renderer, assets *Assets, firstRound bool, keyboardPlayers int) {
	game = &theGame{
		Wnd:        window,
		S:          newScene(),
		R:          renderer,
		background: background{offsetY: 0, height: 1200, width: 1024},
		A:          make(map[string]*sdl.Texture),
		sprites:    make(map[string]*spriteSheet),
		W:          newWave(),
	}

	loadAssetsIntoGame(game, assets)

	if keyboardPlayers == 0 && sdl.NumJoysticks() <= 0 {
		goFatal(errors.New("No players input devices detected"))
	}
	game.players = make([]*entity, 0)
	controllers := setupControllers() //TODO separate player setup from controller setup

	if firstRound {
		game.curState = prestart
		gameAudio.loopIntroMusic()
		for game.curState == prestart {
			frameStartTime := sdl.GetTicks()
			handleSimpleKeyEvents()

			for _, c := range controllers {
				c.handle()
			}

			//prevent enemy wave false start
			game.W.timeOfLastWave = sdl.GetTicks()

			update(game.S)
			render(frameStartTime)
		}
	} else {
		game.curState = running
	}

	gameAudio.loopBackgroundMusic()
	for game.curState == running || game.curState == gameover {
		frameStartTime := sdl.GetTicks()
		handleSimpleKeyEvents()

		for _, c := range controllers {
			c.handle()
		}

		update(game.S)
		render(frameStartTime)
	}
}

func goFatal(err error) {
	_, file, line, _ := runtime.Caller(1)
	fname := filepath.Base(file)
	log.Fatalf("Fatal error at %s:%d: %s\n", fname, line, err)
}

func checkErr(err error) {
	if err != nil {
		goFatal(err)
	}
}

func update(s *scene) {
	for i, _ := range s.enemies {
		enemy := s.enemies[i]
		if enemy == nil {
			continue
		}

		if enemy.dead {
			if enemy.curAnimState == dead {
				s.enemies[i] = nil
			} else {
				enemy.moveDown()
			}
		} else {
			for pe := s.projectiles.Front(); pe != nil; pe = pe.Next() {
				p := pe.Value.(*projectile)

				if p.playerOrigin {
					if p.boundingRect().HasIntersection(enemy.boundingRect()) {
						enemy.onHit(enemy, p.damage)
						s.projectiles.Remove(pe)
					}
				} else {
					for i, _ := range game.players { // player vs enemy proj collision detect
						player := game.players[i]
						if !player.dead && p.boundingRect().HasIntersection(player.boundingRect()) {
							player.onHit(player, p.damage)
							s.projectiles.Remove(pe)
						}
					}
				}

				if pe != nil && !p.isInsideWindow() {
					s.projectiles.Remove(pe)
				}
			}

			target := enemy.findClosestPlayer(game.players)
			if target != nil {
				projDir := diff(target.pos, enemy.pos)
				enemy.fireProjectileInDir(scale(normalized(projDir), enemy.projVelocity))
			}

			for i, _ := range game.players { // player vs enemy collision detect
				player := game.players[i]
				if !player.dead && player.boundingRect().HasIntersection(enemy.boundingRect()) {
					player.onHit(player, 0)
				}
			}
			enemy.moveDown()
			if int32(enemy.pos.y) > windowHeight {
				s.enemies[i] = nil
			}
		}
	}

	//fade out orphaned projectiles
	for pe := s.projectiles.Front(); pe != nil; pe = pe.Next() {
		p := pe.Value.(*projectile)
		if !p.playerOrigin {
			if p.timeToLive > 0 {
				if sdl.GetTicks()-p.timeToLive > 500 {
					p.pos.x = float64(windowWidth + 20.0)
					p.pos.y = float64(windowHeight + 20.0)
					continue
				} else {
					if p.color.A-1 > 0 {
						p.color.A = p.color.A - 2
					}
				}
			} else if p.origin == nil || p.origin.dead {
				p.timeToLive = sdl.GetTicks()
			}
		}
		p.tick()
	}

	for i, pow := range game.S.powerups {
		if pow != nil {
			if pow.isBelowWindow() {
				game.S.powerups[i] = nil
			}

			for j, player := range game.players {
				if !(player.dead && player.hasActivePowerup) {
					if pow.boundingRect().HasIntersection(player.boundingRect()) {
						pow.powerUpBehave.behavior(game.players[j])
						game.S.powerups[i] = nil
					}
				}
			}

			pow.moveDown()
		}
	}

	if game.curState == running && game.W.isNextWaveDue(game.S.enemies, sdl.GetTicks()) {
		game.S.enemies = game.W.generateWave(sdl.GetTicks())
	}

	allPlayersDead := true
	for _, p := range game.players {
		if p.dead == false {
			allPlayersDead = false
			break
		}
	}
	if game.curState == running && allPlayersDead {
		game.curState = gameover
		game.S.newHiscore = insertNewHiscoreIfEligible(s.hiscore, "AAA")
	}

	if game.curState != gameover && game.curState != prestart {
		s.hiscore = s.hiscore + 1
	}
}

func render(frameStartTime uint32) {
	game.R.Clear()
	game.background.scroll()

	game.R.SetDrawColor(0, 0, 0, 255)
	game.R.FillRect(&sdl.Rect{0, 0, windowWidth, windowHeight})

	bgDst := sdl.Rect{0, -game.background.offsetY, windowWidth, game.background.height}
	bgDstCopy := sdl.Rect{0, -game.background.offsetY - game.background.height, windowWidth, game.background.height}
	game.R.Copy(game.background.texture, nil, &bgDst)
	game.R.Copy(game.background.texture, nil, &bgDstCopy)

	//draw players
	for i, p := range game.players {
		if p.curAnimState == moveLeft {
			p.onDrawGFX()
			game.R.CopyEx(p.texture, nil, p.boundingRect(), 0.0, nil, sdl.FLIP_HORIZONTAL)
		} else if p.curAnimState == moveRight {
			p.onDrawGFX()
			game.R.Copy(p.texture, nil, p.boundingRect())
		} else if p.curAnimState == dying {
			if sdl.GetTicks()-p.deathAnim.startTime >= p.deathAnim.duration {
				game.players[i].curAnimState = dead
			} else {
				offset := p.deathAnim.getLoopingAnimFrameOffset()
				clipSize := p.deathAnim.spriteSheet.clipSize
				texSrcRect := sdl.Rect{offset * clipSize, 0, clipSize, clipSize}
				texDstRect := sdl.Rect{p.boundingRect().X, p.boundingRect().Y, clipSize, clipSize}
				game.R.Copy(p.deathAnim.spriteSheet.texture, &texSrcRect, &texDstRect)
			}
		}
	}

	//draw powerups
	for _, pow := range game.S.powerups {
		if pow != nil {
			game.R.Copy(pow.texture, nil, pow.boundingRect())
		}
	}

	//draw enemies
	for i, enemy := range game.S.enemies {
		if enemy == nil {
			continue
		}
		if enemy.curAnimState == moveRight || enemy.curAnimState == moveLeft {
			if sdl.GetTicks()-enemy.hitCountdown < 200 {
				enemy.moveAnim.spriteSheet.texture.SetColorMod(255, 0, 0)
			} else {
				enemy.moveAnim.spriteSheet.texture.SetColorMod(255, 255, 255)
			}

			offset := enemy.moveAnim.getLoopingAnimFrameOffset()
			clipSize := enemy.moveAnim.spriteSheet.clipSize
			texSrcRect := sdl.Rect{offset * clipSize, 0, clipSize, clipSize}
			game.R.Copy(enemy.moveAnim.spriteSheet.texture, &texSrcRect, enemy.boundingRect())
			//			game.R.Copy(enemy.texture, nil, enemy.boundingRect())
		} else if enemy.curAnimState == dying {
			if sdl.GetTicks()-enemy.deathAnim.startTime >= enemy.deathAnim.duration {
				game.S.enemies[i].curAnimState = dead
			} else {
				offset := enemy.deathAnim.getLoopingAnimFrameOffset()
				clipSize := enemy.deathAnim.spriteSheet.clipSize
				texSrcRect := sdl.Rect{offset * clipSize, 0, clipSize, clipSize}
				texDstRect := sdl.Rect{enemy.boundingRect().X, enemy.boundingRect().Y, clipSize, clipSize}
				game.R.Copy(enemy.deathAnim.spriteSheet.texture, &texSrcRect, &texDstRect)
			}
		}
	}

	//draw projectiles
	for e := game.S.projectiles.Front(); e != nil; e = e.Next() {
		p := e.Value.(*projectile)
		game.R.SetDrawBlendMode(sdl.BLENDMODE_BLEND)
		game.R.SetDrawColor(p.color.R, p.color.G, p.color.B, p.color.A)
		game.R.FillRect(p.boundingRect())
	}

	//the game state text texture is manually deallocated as it's not very cacheable
	gameStateText := getGameStateText(game.curState, game.S.hiscore)
	if gameStateText != " " {
		var w, h int
		if game.S.gameStateTextTex != nil {
			game.S.gameStateTextTex.Destroy()
		}
		game.S.gameStateTextTex, w, h = createNonCacheableTextureFromString(gameStateText)
		textDst := sdl.Rect{(windowWidth - int32(w)) / 2, 0, int32(w), int32(h)}
		game.R.Copy(game.S.gameStateTextTex, nil, &textDst)
	}

	if game.curState == gameover || game.curState == prestart {
		for i, hiscore := range currentHiscores {
			hiscoreStr := hiscore.ToString()
			if game.S.hiscore == hiscore.score {
				hiscoreStr = hiscoreStr + " (NEW HISCORE!)"
			}
			hiscoresTex, w2, h2 := createNonCacheableTextureFromString(hiscoreStr)
			textDst2 := sdl.Rect{(windowWidth - int32(w2)) / 2, int32((i+1)*h2 + 50), int32(w2), int32(h2)}
			game.R.Copy(hiscoresTex, nil, &textDst2)
			hiscoresTex.Destroy()
		}
	}

	if game.curState == gameover {
		lastHiscore, w2, h2 := createNonCacheableTextureFromString("Your score was " + iToA(game.S.hiscore))
		textDst3 := sdl.Rect{(windowWidth - int32(w2)) / 2, int32(float32(windowHeight) * float32(0.75)), int32(w2), int32(h2)}
		game.R.Copy(lastHiscore, nil, &textDst3)
		lastHiscore.Destroy()
	}

	if game.curState == prestart {
		detectedPlayersTex, w2, h2 := createNonCacheableTextureFromString(iToA(len(game.players)) + " players detected")
		textDst3 := sdl.Rect{(windowWidth - int32(w2)) / 2, int32(float32(windowHeight) * float32(0.75)), int32(w2), int32(h2)}
		game.R.Copy(detectedPlayersTex, nil, &textDst3)
		detectedPlayersTex.Destroy()
	}

	game.R.Present()

	delta := sdl.GetTicks() - frameStartTime
	if delta < 1000/60 { //if frame was faster than 60 msec...
		sdl.Delay((1000 / 60) - delta) //...sleep for the remaining time
	}
}
