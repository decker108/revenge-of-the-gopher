package main

import (
	"math/rand"
	"time"

	"github.com/veandco/go-sdl2/sdl"
)

func (s *scene) addPowerUp(x, y float64) {
	powerup := newPowerUpAtPos(x, y, randomizeBehaviorType())
	game.S.powerups = append(game.S.powerups, powerup)
	game.S.lastPowerUpTime = sdl.GetTicks()
	gameAudio.playPowerUpSpawned()
}

func (s *scene) isNextPowerUpDue() bool {
	return rand.Intn(100) <= 25
}

type powerUpBehaviorType int

const (
	fastFireRate powerUpBehaviorType = 0
	hiDamage     powerUpBehaviorType = 1
	reviveAll    powerUpBehaviorType = 2
	immortality  powerUpBehaviorType = 3
	scatterShot  powerUpBehaviorType = 4
)

func randomizeBehaviorType() powerUpBehaviorType {
	randNum := rand.Intn(100)
	if randNum >= 0 && randNum < 25 {
		return fastFireRate
	}
	if randNum >= 25 && randNum < 50 {
		return hiDamage
	}
	if randNum >= 50 && randNum < 75 {
		return scatterShot
	}
	if randNum >= 75 && randNum < 87 {
		return immortality
	}
	if randNum >= 87 && randNum < 100 {
		return reviveAll
	}
	return fastFireRate
}

func getBehaviorClosure(behaviorType powerUpBehaviorType) func(e *entity) {
	switch behaviorType {
	case fastFireRate:
		return func(e *entity) {
			if e.hasActivePowerup {
				return
			}
			e.hasActivePowerup = true
			game.S.hiscore += 500
			gameAudio.playPowerUpActive()

			origProjColor := e.projColor
			origROF := e.rateOfFire
			e.rateOfFire = 100
			e.projColor = sdl.Color{R: 255, G: 255, B: 0, A: 255}
			go func() {
				time.Sleep(time.Second * 10)
				e.rateOfFire = origROF
				e.projColor = origProjColor
				e.hasActivePowerup = false
			}()
		}
	case hiDamage:
		return func(e *entity) {
			if e.hasActivePowerup {
				return
			}
			e.hasActivePowerup = true
			game.S.hiscore += 500
			gameAudio.playPowerUpActive()

			origProjColor := e.projColor
			e.projDamage = 85
			e.projColor = sdl.Color{R: 255, G: 0, B: 0, A: 255}
			go func() {
				time.Sleep(time.Second * 10)
				e.projDamage = 20
				e.projColor = origProjColor
				e.hasActivePowerup = false
			}()
		}
	case reviveAll:
		return func(e *entity) {
			game.S.hiscore += 1000
			gameAudio.playPowerUpActive()

			for i, _ := range game.players {
				if game.players[i].dead {
					game.players[i].dead = false
					game.players[i].curAnimState = moveRight
					immortalityPowerUpBehavior(game.players[i], 0*time.Second, 3*time.Second)
				}
			}
		}
	case immortality:
		return func(e *entity) {
			if e.hasActivePowerup {
				return
			}
			game.S.hiscore += 500
			gameAudio.playPowerUpActive()

			immortalityPowerUpBehavior(e, 7*time.Second, 3*time.Second)
		}
	case scatterShot:
		return func(e *entity) {
			if e.hasActivePowerup {
				return
			}
			e.hasActivePowerup = true
			game.S.hiscore += 500
			gameAudio.playPowerUpActive()

			origOnFire, origVelo, origRoF, origDmg := e.onFire, e.projVelocity, e.rateOfFire, e.projDamage
			scattershotPowerUpBehaviour(e)

			go func() {
				time.Sleep(time.Second * 10)
				e.onFire, e.projVelocity, e.rateOfFire = origOnFire, origVelo, origRoF
				e.projDamage = origDmg
				e.hasActivePowerup = false
			}()
		}
	default:
		return func(e *entity) {
			game.S.hiscore += 500
		}
	}
}

func scattershotPowerUpBehaviour(e *entity) {
	e.projVelocity = 1.2
	e.rateOfFire = 450
	e.projDamage = 15
	e.onFire = func(e *entity, v vector) {
		now := sdl.GetTicks()
		if (now - e.lastProjectileTick) > e.rateOfFire {
			for i := 0; i < 8; i++ {
				randomDir := vector{e.pos.x - float64(rand.Intn(60)-30), e.pos.y + 40}
				projDir := scale(normalized(diff(e.pos, randomDir)), float64(rand.Intn(10)+5))
				game.S.addProjectile(e, projDir)
			}
			gameAudio.playLaser()
			e.lastProjectileTick = now
		}
	}
}

func immortalityPowerUpBehavior(e *entity, stableDuration, fadeDuration time.Duration) {
	e.hasActivePowerup = true
	origOnHit := e.onHit
	e.onHit = func(e *entity, damage int32) {
		return
	}
	go func() {
		e.onDrawGFX = func() {
			e.texture.SetColorMod(255, 0, 0)
		}
		time.Sleep(stableDuration)
		flipColor := true
		flipColorTime := sdl.GetTicks()
		e.onDrawGFX = func() {
			if sdl.GetTicks()-flipColorTime > 200 {
				if flipColor {
					e.texture.SetColorMod(255, 0, 0)
					flipColor = !flipColor
					flipColorTime = sdl.GetTicks()
				} else {
					e.texture.SetColorMod(255, 255, 255)
					flipColor = !flipColor
					flipColorTime = sdl.GetTicks()
				}
			}
		}
		time.Sleep(fadeDuration)
		e.onDrawGFX = func() {
			return
		}
		e.texture.SetColorMod(255, 255, 255)
		e.onHit = origOnHit
		e.hasActivePowerup = false
	}()
}

type powerUpBehavior struct {
	behaviorType powerUpBehaviorType
	behavior     func(*entity)
}

func newPowerUp() *entity {
	x, y := float64(rand.Int31n(windowWidth-50)), 0.0
	return newPowerUpAtPos(x, y, randomizeBehaviorType())
}

func newPowerUpAtPos(x, y float64, bType powerUpBehaviorType) *entity {
	powerup := entity{
		pos:          vector{x, y},
		width:        50,
		height:       50,
		life:         999,
		velocity:     3,
		score:        500,
		curAnimState: moveLeft,
		hostile:      false,
		texture:      game.A[powerUp1ImgPath],
	}
	powerup.powerUpBehave = powerUpBehavior{
		behaviorType: bType,
		behavior:     getBehaviorClosure(bType),
	}
	if bType == hiDamage {
		powerup.texture = game.A[powerUp2ImgPath]
	}
	if bType == reviveAll {
		powerup.texture = game.A[powerUp3ImgPath]
	}
	if bType == immortality {
		powerup.texture = game.A[powerUp4ImgPath]
	}
	if bType == scatterShot {
		powerup.texture = game.A[powerUp5ImgPath]
	}
	return &powerup
}
