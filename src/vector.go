package main

import "math"

type vector struct {
	x, y float64
}

func (v vector) length() float64 {
	return math.Sqrt(v.x*v.x + v.y*v.y)
}

func translate(pos, dir vector) vector {
	return vector{
		x: pos.x + dir.x,
		y: pos.y + dir.y,
	}
}

func add(v1, v2 vector) vector {
	return vector{
		x: v1.x + v2.x,
		y: v1.y + v2.y,
	}
}

func diff(v1, v2 vector) vector {
	return vector{
		x: v1.x - v2.x,
		y: v1.y - v2.y,
	}
}

func scale(v vector, s float64) vector {
	return vector{
		x: v.x * s,
		y: v.y * s,
	}
}

func normalized(v vector) vector {
	l := v.length()
	return vector{
		x: v.x / l,
		y: v.y / l,
	}
}
