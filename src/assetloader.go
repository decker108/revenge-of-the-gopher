package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/sdl_image"
	"github.com/veandco/go-sdl2/sdl_ttf"
)

var (
	fontImgPath         = "assets/Consolas.ttf"
	bgImgPath           = "assets/circuitboard.png"
	bgImgWidePath       = "assets/circuitboard_wide.png"
	powerUp1ImgPath     = "assets/powerup1.png"
	powerUp2ImgPath     = "assets/powerup2.png"
	powerUp3ImgPath     = "assets/powerup3.png"
	powerUp4ImgPath     = "assets/powerup4.png"
	powerUp5ImgPath     = "assets/powerup5.png"
	enemy1FramesImgPath = "assets/computer-bug-1.png"
	exploFramesImgPath  = "assets/explosion_sheet.png"
)

type Assets struct {
	playerTextures    []*sdl.Texture
	enemy1SpriteSheet *spriteSheet
	bgTex             *sdl.Texture
	consolasFont      *ttf.Font
	exploSpriteSheet  *spriteSheet
	powerUp1Tex       *sdl.Texture
	powerUp2Tex       *sdl.Texture
	powerUp3Tex       *sdl.Texture
	powerUp4Tex       *sdl.Texture
	powerUp5Tex       *sdl.Texture
}

func setupAssets(renderer *sdl.Renderer, windowedMode bool) *Assets {
	//TODO replace this with single texture and color modulation
	//this is gonna take some direct pixel manipulation though....
	playerTextures := make([]*sdl.Texture, 8)
	for i := 0; i < 8; i++ {
		idx := iToA(i + 1)
		gopher1Surface, err := img.Load("assets/gopher" + idx + ".png")
		checkErr(err)
		gopherTex, err := renderer.CreateTextureFromSurface(gopher1Surface)
		checkErr(err)
		gopher1Surface.Free()
		playerTextures[i] = gopherTex
	}

	exploFramesSurface, err := img.Load(exploFramesImgPath)
	checkErr(err)
	exploFramesTex, err := renderer.CreateTextureFromSurface(exploFramesSurface)
	checkErr(err)
	exploFramesSurface.Free()
	exploSpriteSheet := newSpriteSheet(exploFramesTex, 100, 2, 1)

	enemy1FramesSurface, err := img.Load(enemy1FramesImgPath)
	checkErr(err)
	enemy1FramesTex, err := renderer.CreateTextureFromSurface(enemy1FramesSurface)
	checkErr(err)
	enemy1FramesSurface.Free()
	enemy1SpriteSheet := newSpriteSheet(enemy1FramesTex, 256, 3, 1)

	var backgroundSurface *sdl.Surface
	if windowedMode {
		backgroundSurface, err = img.Load(bgImgPath)
	} else {
		backgroundSurface, err = img.Load(bgImgWidePath)
	}
	checkErr(err)
	backgroundTexture, err := renderer.CreateTextureFromSurface(backgroundSurface)
	checkErr(err)
	backgroundSurface.Free()

	powerUpImgs := [5]string{powerUp1ImgPath, powerUp2ImgPath, powerUp3ImgPath, powerUp4ImgPath, powerUp5ImgPath}
	var powerUpTexs [5]*sdl.Texture
	for i, imgPath := range powerUpImgs {
		powerUp1Surface, err := img.Load(imgPath)
		checkErr(err)
		powerUp1Texture, err := renderer.CreateTextureFromSurface(powerUp1Surface)
		checkErr(err)
		powerUp1Surface.Free()
		powerUpTexs[i] = powerUp1Texture
	}

	consolasFont, err := ttf.OpenFont(fontImgPath, 38)
	checkErr(err)

	return &Assets{
		playerTextures,
		enemy1SpriteSheet,
		backgroundTexture,
		consolasFont,
		exploSpriteSheet,
		powerUpTexs[0],
		powerUpTexs[1],
		powerUpTexs[2],
		powerUpTexs[3],
		powerUpTexs[4],
	}
}

func loadAssetsIntoGame(game *theGame, assets *Assets) {
	for i := 0; i < 8; i++ {
		idx := iToA(i + 1)
		game.A["assets/gopher"+idx+".png"] = assets.playerTextures[i]
	}
	game.sprites[enemy1FramesImgPath] = assets.enemy1SpriteSheet
	game.sprites[exploFramesImgPath] = assets.exploSpriteSheet
	game.A[powerUp1ImgPath] = assets.powerUp1Tex
	game.A[powerUp2ImgPath] = assets.powerUp2Tex
	game.A[powerUp3ImgPath] = assets.powerUp3Tex
	game.A[powerUp4ImgPath] = assets.powerUp4Tex
	game.A[powerUp5ImgPath] = assets.powerUp5Tex
	game.background.texture = assets.bgTex
	game.defaultFont = assets.consolasFont
}
