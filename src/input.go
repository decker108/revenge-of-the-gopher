package main

import (
	"errors"
	"log"

	"github.com/veandco/go-sdl2/sdl"
)

type controller interface {
	handle()
}

type joystickController struct {
	joystick *sdl.Joystick
	entity   *entity
}

type dualStickJoystickController struct {
	joystick *sdl.Joystick
	entity   *entity
}

type xbox360Controller struct {
	gamecontroller *sdl.GameController
	entity         *entity
}

type keyboardController struct {
	entity *entity
}

func (c *joystickController) handle() {
	e := c.entity
	left, right, up, down := false, false, false, false
	if c.joystick.GetAxis(1) < 0 {
		up = true
	}
	if c.joystick.GetAxis(1) > 0 {
		down = true
	}
	if c.joystick.GetAxis(0) < 0 {
		left = true
	}
	if c.joystick.GetAxis(0) > 0 {
		right = true
	}
	if left || right || up || down {
		e.move(left, right, up, down)
	}

	if c.joystick.GetButton(1) != 0 {
		e.fireProjectile()
	}
}

func (c *dualStickJoystickController) handle() {
	e := c.entity
	left, right, up, down := false, false, false, false
	dpad := c.joystick.GetHat(0)
	if dpad == 1 {
		up = true
	}
	if dpad == 2 {
		right = true
	}
	if dpad == 4 {
		down = true
	}
	if dpad == 8 {
		left = true
	}
	if left || right || up || down {
		e.move(left, right, up, down)
	}

	if c.joystick.GetButton(1) != 0 {
		e.fireProjectile()
	}
}

func (c *xbox360Controller) handle() {
	e := c.entity
	left, right, up, down := false, false, false, false
	if c.gamecontroller.GetButton(sdl.CONTROLLER_BUTTON_DPAD_UP) != 0 {
		up = true
	}
	if c.gamecontroller.GetButton(sdl.CONTROLLER_BUTTON_DPAD_DOWN) != 0 {
		down = true
	}
	if c.gamecontroller.GetButton(sdl.CONTROLLER_BUTTON_DPAD_LEFT) != 0 {
		left = true
	}
	if c.gamecontroller.GetButton(sdl.CONTROLLER_BUTTON_DPAD_RIGHT) != 0 {
		right = true
	}
	if left || right || up || down {
		e.move(left, right, up, down)
	}

	if c.gamecontroller.GetButton(sdl.CONTROLLER_BUTTON_B) != 0 {
		e.fireProjectile()
	}
}

func (c *keyboardController) handle() {
	e := c.entity
	left, right, up, down := false, false, false, false
	keystate := sdl.GetKeyboardState()
	if keystate[sdl.SCANCODE_W] != 0 {
		up = true
	}
	if keystate[sdl.SCANCODE_S] != 0 {
		down = true
	}
	if keystate[sdl.SCANCODE_A] != 0 {
		left = true
	}
	if keystate[sdl.SCANCODE_D] != 0 {
		right = true
	}
	if left || right || up || down {
		e.move(left, right, up, down)
	}

	if keystate[sdl.SCANCODE_SPACE] != 0 {
		e.fireProjectile()
	}
}

func handleSimpleKeyEvents() {
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			game.curState = quit
		case *sdl.KeyUpEvent:
			switch t.Keysym.Sym {
			case sdl.K_ESCAPE:
				game.curState = quit
			case sdl.K_RETURN:
				if game.curState == prestart {
					game.curState = running
				}
				if game.curState == gameover {
					game.curState = restart
				}
			}
		}
	}
}

func setupControllers() []controller { //TODO separate player setup from controller setup
	controllers := make([]controller, 0)

	kbdPlayer := newPlayer()
	idx := iToA(1)
	kbdPlayer.texture = game.A["assets/gopher"+idx+".png"]
	controllers = append(controllers, &keyboardController{
		entity: kbdPlayer,
	})
	game.players = append(game.players, kbdPlayer)

	for i := 0; i < sdl.NumJoysticks(); i++ {
		joystick := sdl.JoystickOpen(i)
		if joystick == nil {
			goFatal(errors.New("Could not find joystick " + iToA(i)))
		}
		log.Println("Found joystick #", i, "name:", sdl.JoystickNameForIndex(i),
			"with", joystick.NumButtons(), "buttons")

		p := newPlayerAtPos(float64(43+(i+1)*100), float64(700))
		idx := iToA(i + 2)
		p.texture = game.A["assets/gopher"+idx+".png"]
		if joystick.Name() == "Microsoft X-Box 360 pad" {
			if sdl.IsGameController(i) {
				gamectrl := sdl.GameControllerOpen(i)
				if gamectrl == nil {
					goFatal(errors.New("Error initializing X-box 360 controller"))
				}
				controllers = append(controllers,
					&xbox360Controller{
						entity:         p,
						gamecontroller: gamectrl,
					})
			}
		} else {
			controllers = append(controllers,
				&joystickController{
					entity:   p,
					joystick: joystick,
				})
		}

		game.players = append(game.players, p)
	}

	return controllers
}
