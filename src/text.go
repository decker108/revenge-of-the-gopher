package main

import (
	"fmt"
	"strconv"

	"github.com/veandco/go-sdl2/sdl"
)

var textCache = make(map[string]*sdl.Texture)

func createCacheableTextureFromString(textString string) (*sdl.Texture, int, int) {
	if textTex, isPresent := textCache[textString]; isPresent {
		_, _, w, h, _ := textTex.Query()
		return textTex, w, h
	}
	surface := game.defaultFont.RenderText_Solid(textString, sdl.Color{R: 255, G: 255, B: 255, A: 0})
	texture, err := game.R.CreateTextureFromSurface(surface)
	if err != nil {
		goFatal(err)
	}
	surface.Free()
	_, _, w, h, _ := texture.Query()
	textCache[textString] = texture
	return texture, w, h
}

func createNonCacheableTextureFromString(textString string) (*sdl.Texture, int, int) {
	surface := game.defaultFont.RenderText_Solid(textString, sdl.Color{R: 255, G: 255, B: 255, A: 0})
	texture, err := game.R.CreateTextureFromSurface(surface)
	if err != nil {
		goFatal(err)
	}
	surface.Free()
	_, _, w, h, _ := texture.Query()
	return texture, w, h
}

func getGameStateText(curState gameState, hiscore int) string {
	switch curState {
	case prestart:
		return "Press ENTER to start"
	case running:
		return fmt.Sprintf("%v", hiscore)
	case gameover:
		return "Game over - Press ENTER to play again"
	default:
		return " "
	}
}

func iToA(i int) string {
	return strconv.Itoa(i)
}

func handleTextInput() {
	//TODO use this func for hiscore name input
}
