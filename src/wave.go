package main

import (
	"math"
	"math/rand"

	"github.com/veandco/go-sdl2/sdl"
)

type wave struct {
	timeOfLastWave      uint32
	waveInterval        uint32
	waveSize            int
	waveIncreaseCounter int
	waveIncreaseMod     int
	waveNumber          int
}

func newWave() *wave {
	return &wave{
		timeOfLastWave:      sdl.GetTicks(),
		waveInterval:        3500,
		waveSize:            1,
		waveIncreaseCounter: 1,
		waveIncreaseMod:     3,
		waveNumber:          1,
	}
}

func (w *wave) isNextWaveDue(enemies []*entity, now uint32) bool {
	for _, e := range enemies {
		if e != nil {
			return false
		}
	}
	if now-w.timeOfLastWave > w.waveInterval {
		return true
	}
	return false
}

func (w *wave) generateWave(now uint32) []*entity {
	w.timeOfLastWave = now

	enemies := make([]*entity, 0)
	for i := 0; i < w.waveSize; i++ {
		//build enemies
		enemy := newEnemy()

		//randomize x positions
		enemy.pos.x = float64(rand.Intn(int(1024 - enemy.width)))
		enemy.pos.y = float64(-100 * i)
		enemy.startX = enemy.pos.x
		enemy.phase = rand.Float64() * 2 * math.Pi

		if math.Mod(float64(w.waveNumber), float64(3)) == 0 {
			enemy.movtPattern = sine
		}

		enemies = append(enemies, enemy)
	}

	if game.curState == running {
		if math.Mod(float64(w.waveIncreaseCounter), float64(w.waveIncreaseMod)) == 0 {
			w.waveSize = w.waveSize + 1
			w.waveIncreaseMod = w.waveIncreaseMod + 1
			w.waveIncreaseCounter = 0
		}
		w.waveIncreaseCounter = w.waveIncreaseCounter + 1
	}

	w.waveNumber += 1

	return enemies
}

func loadWaveFile(filePath string) {
	//TODO implement JSON parser here
}
