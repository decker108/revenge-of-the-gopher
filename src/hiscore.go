package main

import (
	"bufio"
	"fmt"
	"os"
)

type hiscore struct {
	name  string
	score int
}

var (
	initialHiscores [5]hiscore
	currentHiscores [5]hiscore
	hiscoreFilename = "hiscores"
)

func (hi *hiscore) ToString() string {
	return hi.name + " " + iToA(hi.score)
}

func loadHiscores() {
	f, err := createHiscoreFileIfNotExists()
	checkForError(err)
	reader := bufio.NewReader(f)
	for i := 0; i < 5; i++ {
		b4, _, err := reader.ReadLine()
		checkForError(err)
		var iscore int
		var iname string
		_, err = fmt.Sscanf(string(b4), "%s %d", &iname, &iscore)
		checkForError(err)
		initialHiscores[i] = hiscore{name: iname, score: iscore}
	}
	copy(currentHiscores[:], initialHiscores[:])

	err = f.Close()
	checkForError(err)
}

func createHiscoreFileIfNotExists() (*os.File, error) {
	f, err := os.Open(hiscoreFilename)
	if err != nil && os.IsNotExist(err) {
		f, err = os.Create(hiscoreFilename)
		checkForError(err)
		for i := 5; i > 0; i-- {
			f.WriteString(fmt.Sprintln("AAA ", i))
			f.Close()
		}
	} else {
		checkForError(err)
	}
	return os.Open(hiscoreFilename)
}

func checkForError(err error) {
	if err != nil {
		panic(err)
	}
}

func writeHiscores() {
	file, err := os.Create(hiscoreFilename)
	checkForError(err)
	defer file.Close()

	writer := bufio.NewWriter(file)
	for i := 0; i < 5; i++ {
		outStr := currentHiscores[i].name + " " + iToA(currentHiscores[i].score) + "\n"
		_, err := writer.WriteString(outStr)
		checkForError(err)
	}

	err = writer.Flush()
	checkForError(err)
}

func diffHiscores() bool {
	for i, initScore := range initialHiscores {
		if currentHiscores[i].score != initScore.score ||
			currentHiscores[i].name != initScore.name {
			return true
		}
	}
	return false
}

func insertNewHiscoreIfEligible(newScore int, newName string) bool {
	length := len(currentHiscores)
	for i, hi := range currentHiscores {
		if newScore > hi.score {
			if i < length-1 {
				nextToEndSlice := make([]hiscore, length)
				copy(nextToEndSlice, currentHiscores[i:length-1])
				for j, k := 0, i+1; j < len(nextToEndSlice) && k < length; j, k = j+1, k+1 {
					currentHiscores[k] = nextToEndSlice[j]
				}
			}
			currentHiscores[i] = hiscore{score: newScore, name: newName}
			return true
		}
	}
	return false
}

//func main() {
//	loadHiscores()
//	fmt.Println(initialHiscores)
//	insertNewHiscoreIfEligible(205, "OOE")
//	fmt.Println("diff:", diffHiscores())
//	writeHiscores()
//}
